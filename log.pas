
unit Log;

interface

uses
  SysUtils, Classes;

procedure ToLog(const ALine: string; const AFileNum: integer = 0); overload;
procedure ToLog(const ALines: TStrings; const APrefix: string; const AFileNum: integer = 0); overload;

implementation

var
  LFile: array[0..1] of text;

procedure CreateFiles;
var
  LDateTime: string;
  LName: array[0..1] of string;
  LFileNum: integer;
begin
  LDateTime := FormatDateTime('yymmddhhnnss', Now);
  for LFileNum := 0 to 1 do
    LName[LFileNum] := Format('%s-%s-%d.log', [ChangeFileExt(ExtractFileName(ParamStr(0)), ''), LDateTime, LFileNum]);
  for LFileNum := 0 to 1 do
  begin
    Assign(LFile[LFileNum], LName[LFileNum]);
    Rewrite(LFile[LFileNum]);
  end;
end;

procedure FreeFiles;
var
  LFileNum: integer;
begin
  for LFileNum := 0 to 1 do
    Close(LFile[LFileNum]);
end;

procedure ToLog(const ALine: string; const AFileNum: integer);
begin
{$IFDEF DEBUG}
  if AFileNum = 0 then
    WriteLn(LFile[AFileNum], FormatDateTime('yyyy/mm/dd hh:nn:ss:zzz', Now), ALine)
  else
    WriteLn(LFile[AFileNum], ALine);
  Flush(LFile[AFileNum]);
{$ENDIF}
end;

procedure ToLog(const ALines: TStrings; const APrefix: string; const AFileNum: integer);
var
  i: integer;
  s: string;
begin
{$IFDEF DEBUG}
  if AFileNum = 0 then
    s := FormatDateTime('yyyy/mm/dd hh:nn:ss:zzz', Now)
  else
    s := EmptyStr;
  for i := 0 to Pred(ALines.Count) do
    WriteLn(LFile[AFileNum], s, APrefix, ALines[i]);
  Flush(LFile[AFileNum]);
{$ENDIF}
end;

initialization
{$IFDEF DEBUG}
  CreateFiles;
{$ENDIF}

finalization
{$IFDEF DEBUG}
  FreeFiles;
{$ENDIF}

end.
