debug:
	fpc -B -FUunits durandal.pas -odurandal64 -Mobjfpc -Sah -dDEBUG -ghl -Cr
release:
	fpc -B -FUunits durandal.pas -odurandal64 -Mobjfpc -Sh -dRELEASE -O3 -CX -XX -Xs
random:
	fpc -B -FUunits durandal.pas -orandom64 -Mobjfpc -Sh -dRELEASE -O3 -CX -XX -Xs -dRANDOM_MOVER
test:
	rm -f *.log
	expect test.exp
	geany *1.log
