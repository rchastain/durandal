# Durandal

## Overview

*Durandal* is a very simple UCI chess engine, able to play traditional chess, Fischer random chess, Capablanca chess and Capablanca random chess.

Despite his heroic name, *Durandal* is not a very formidable opponent. You should manage to beat him easily.

## Option

*Durandal* plays almost instantly. If you wish, you can change this behaviour by starting the engine with parameter `-w` or `--wait`.

## Build

To build *Durandal*, you need the Free Pascal compiler.

To build the *release* version, you can use the following command line:

    fpc durandal.pas -Mobjfpc -Sh -dRELEASE -O3 -CX -XX -Xs

To build the *debug* version:

    fpc durandal.pas -Mobjfpc -Sah -dDEBUG -ghl

If you need a pure random mover:

    fpc durandal.pas -Mobjfpc -Sh -dRANDOM_MOVER

## Logo

Norbert Raimund Leisner made beautiful logos for *Durandal*.

![alt text](https://raw.githubusercontent.com/rchastain/durandal/master/logos/durandal-2.bmp)

## Author

*Durandal* is written for the Free Pascal compiler by Roland Chastain.

## Homepage

https://gitlab.com/rchastain/durandal
