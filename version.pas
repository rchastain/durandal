
const
  CAppName  = 'Durandal';
  CVersion  = '0.1.3';
  CAuthor   = 'Roland Chastain';
  CBuild    = {$I %DATE%} + ' ' + {$I %TIME%};
  CCompiler = 'Free Pascal ' + {$I %FPCVERSION%} + ' ' + {$I %FPCTARGETCPU%} + '-' + {$I %FPCTARGETOS%};
  CAppInfo  = CAppName + ' ' + CVersion + ' ' + CBuild + ' ' + CCompiler;
