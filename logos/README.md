
Logos for *Durandal* by Norbert Raimund Leisner.

![alt text](https://gitlab.com/rchastain/durandal/-/raw/master/logos/durandal-1.bmp)

![alt text](https://gitlab.com/rchastain/durandal/-/raw/master/logos/durandal-2.bmp)

![alt text](https://gitlab.com/rchastain/durandal/-/raw/master/logos/durandal-3.bmp)

![alt text](https://gitlab.com/rchastain/durandal/-/raw/master/logos/durandal-4.bmp)
