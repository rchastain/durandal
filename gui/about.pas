unit about;

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  DefaultTranslator;

type

  { TForm2 }

  TForm2 = class(TForm)
    btClose: TButton;
    imImage: TImage;
    mmText: TMemo;
    procedure btCloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

  public

  end;

implementation

{$R *.lfm}

resourcestring
  rsAbout =
    'Simple GUI for Durandal chess program.' + LineEnding +
    '' + LineEnding +
    'Durandal GUI is a Lazarus project.';

{ TForm2 }

procedure TForm2.btCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TForm2.FormCreate(Sender: TObject);
begin
  mmText.Text := rsAbout;
end;

end.

