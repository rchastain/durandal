unit main;

interface

uses
  LCLIntf, LCLType,
  SysUtils, Variants, Classes, Graphics,
  Controls, Forms, Dialogs, StdCtrls, ExtCtrls, Menus, DefaultTranslator,
  LResources;

type

  { TForm1 }

  TForm1 = class(TForm)
    lbMessage: TLabel;
    miComputerPlaysWhite: TMenuItem;
    miComputerPlaysBlack: TMenuItem;
    miComputerDoesNotPlay: TMenuItem;
    miNewGame: TMenuItem;
    miOptions: TMenuItem;
    mmMenu: TMainMenu;
    miApplication: TMenuItem;
    miHelp: TMenuItem;
    miAbout: TMenuItem;
    miQuit: TMenuItem;
    pbChessboard: TPaintBox;
    pnChessboard: TPanel;
    Separator1: TMenuItem;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure miComputerPlaysClick(Sender: TObject);
    procedure miNewGameClick(Sender: TObject);
    procedure pbChessboardPaint(Sender: TObject);
    procedure pbChessboardMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure pbChessboardMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure pbChessboardMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormDestroy(Sender: TObject);
    procedure miQuitClick(Sender: TObject);
    procedure miAboutClick(Sender: TObject);
    procedure pnChessboardPaint(Sender: TObject);
  private
    { Déclarations privées }
    FBitmap: TBitmap;
    FGameOver: boolean;
    procedure DrawEmptyChessboard;
    procedure CreatePieces(const AFen: string);
    procedure FreePieces;
    procedure AfterMove;
    procedure ComputerMove;
    function IsLegal(const AMove: string): boolean;
  public
    { Déclarations publiques }
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

uses
  Utils, Chess, About;

resourcestring
  rsWhiteToMove = 'White to move.';
  rsBlackToMove = 'Black to move.';
  rsCheck = 'Check!';
  rsCheckmate = 'Checkmate!';
  rsStalemate = 'Stalemate!';
  rsWaiting = 'Please wait...';

const
  CScale = 36;
  CCapablancaStartPos = 'rnabqkbcnr/pppppppppp/10/10/10/10/PPPPPPPPPP/RNABQKBCNR w KQkq - 0 1';
  GRAB = 1;
  GRABBING = 2;
  
var
  LDragging: boolean = FALSE;
  LDX, LDY: integer;
  LPieces: array[0..39] of record
    fg, bg: TBitmap;
    xx, yy: integer;
  end;
  LPieceIndex: integer = 0;
  LUserMove: string;
  LPos: TPosition;
  LFen: ansistring;
  LMoveList: TStrArray;
  LOldX, LOldY: integer;

type
  TGameState = (gsWhiteToMove, gsBlackToMove, gsCheckmate, gsStalemate);

var
  LState: TGameState;

procedure TForm1.AfterMove;
var
  LIndex: integer;
  LCheck: boolean;
begin
  GenLegalMoves(LMoveList, LPos);
  
 {for LIndex := 0 to High(LMoveList) do
    Write(LMoveList[LIndex], ' ');
  WriteLn;}

  LCheck := IsCheck(LPos);

  WriteLn('LCheck = ', LCheck);

  if Length(LMoveList) > 0 then
    LState := TGameState(Ord(LPos.color))
  else    
    if LCheck then
      LState := gsCheckmate
    else
      LState := gsStalemate;
  
  WriteLn('LState = ', LState);

  FGameOver := LState in [gsCheckmate, gsStalemate];

  if FGameOver then
    case LState of
      gsCheckmate: lbMessage.Caption := rsCheckmate;
      gsStalemate: lbMessage.Caption := rsStalemate;
    end
  else
  begin
    if LCheck then lbMessage.Caption := rsCheck + ' ' else lbMessage.Caption := EmptyStr;
    if LPos.color then
      lbMessage.Caption := lbMessage.Caption + rsBlackToMove
    else
      lbMessage.Caption := lbMessage.Caption + rsWhiteToMove;
  end;

  if (miComputerPlaysWhite.Checked and not LPos.color
  or  miComputerPlaysBlack.Checked and LPos.color)
  and not FGameOver then
  begin
    lbMessage.Caption := lbMessage.Caption + ' ' + rsWaiting;
    ComputerMove;
  end;
end;

procedure TForm1.ComputerMove;
var
  LComputerMove: string;
  i, x, y: integer;
  LRookCurrentX, LRookCurrentY, LKingTargetX, LRookTargetX, LRookIndex: integer;
  LCapturedPawnX, LCapturedPawnY: integer;
  LPromotionType: char;
begin
  Application.ProcessMessages;
  Sleep(200);

  LComputerMove := BestMove(LPos, 500);
  WriteLn('LComputerMove = ', LComputerMove);
  for i := 0 to 39 do
    if  SquareToStr(LPieces[i].xx div CScale, 7 - LPieces[i].yy div CScale) = Copy(LComputerMove, 1, 2) then
    begin
      LPieceIndex := i;
     {WriteLn('LPieceIndex = ', LPieceIndex);}
      Break;
    end;
      
  if IsCastling(LPos, LComputerMove, LRookCurrentX, LRookCurrentY, LKingTargetX, LRookTargetX) then
  begin
  { Roque. }

    LRookCurrentY := 7 - LRookCurrentY;

  { Déplacement de l'image du roi. }
    FBitmap.Canvas.Draw(LPieces[LPieceIndex].xx, LPieces[LPieceIndex].yy, LPieces[LPieceIndex].bg);

    LPieces[LPieceIndex].xx := CScale * LKingTargetX;
    LPieces[LPieceIndex].yy := CScale * LRookCurrentY;

    LPieces[LPieceIndex].bg.Canvas.CopyRect(
      Rect(0, 0, CScale, CScale),
      FBitmap.Canvas,
      Rect(LPieces[LPieceIndex].xx, LPieces[LPieceIndex].yy, LPieces[LPieceIndex].xx + CScale, LPieces[LPieceIndex].yy + CScale)
    );
    FBitmap.Canvas.Draw(LPieces[LPieceIndex].xx, LPieces[LPieceIndex].yy, LPieces[LPieceIndex].fg);

  { Recherche de l'image de la tour. }
    for i := 0 to 39 do
      if  (LPieces[i].xx = CScale * LRookCurrentX)
      and (LPieces[i].yy = CScale * LRookCurrentY) then
      begin
        LRookIndex := i;
        Break;
      end;

  { Déplacement de l'image de la tour. }
    FBitmap.Canvas.Draw(LPieces[LRookIndex].xx, LPieces[LRookIndex].yy, LPieces[LRookIndex].bg);

    LPieces[LRookIndex].xx := CScale * LRookTargetX;
    LPieces[LRookIndex].yy := CScale * LRookCurrentY;

    LPieces[LRookIndex].bg.Canvas.CopyRect(
      Rect(0, 0, CScale, CScale),
      FBitmap.Canvas,
      Rect(LPieces[LRookIndex].xx, LPieces[LRookIndex].yy, LPieces[LRookIndex].xx + CScale, LPieces[LRookIndex].yy + CScale)
    );
    FBitmap.Canvas.Draw(LPieces[LRookIndex].xx, LPieces[LRookIndex].yy, LPieces[LRookIndex].fg);

    InvalidateRect(Handle, nil, FALSE);
  end else
  begin
    FBitmap.Canvas.Draw(LPieces[LPieceIndex].xx, LPieces[LPieceIndex].yy, LPieces[LPieceIndex].bg);
    StrToSquare(Copy(LComputerMove, 3, 2), x, y); y := 7 - y;
    LPieces[LPieceIndex].xx := CScale * x;
    LPieces[LPieceIndex].yy := CScale * y;

    if IsEnPassantCapture(LPos, LComputerMove, LCapturedPawnX, LCapturedPawnY) then
    begin
    { Prise en passant. }
      LCapturedPawnY := 7 - LCapturedPawnY;
    { Effacement de l'image du pion capturé. }
      for i := 0 to 39 do
        if  (LPieces[i].xx = CScale * LCapturedPawnX)
        and (LPieces[i].yy = CScale * LCapturedPawnY) then
        begin
          FBitmap.Canvas.Draw(LPieces[i].xx, LPieces[i].yy, LPieces[i].bg);
          LPieces[i].xx := -1;
          LPieces[i].yy := -1;
          Break;
        end;
    end else
      for i := 0 to 39 do
        if (i <> LPieceIndex)
        and (LPieces[i].xx = LPieces[LPieceIndex].xx)
        and (LPieces[i].yy = LPieces[LPieceIndex].yy) then
        begin
          FBitmap.Canvas.Draw(LPieces[i].xx, LPieces[i].yy, LPieces[i].bg);
          LPieces[i].xx := -1;
          LPieces[i].yy := -1;
          Break;
        end;

    if IsPromotion(LPos, LComputerMove) then
    begin
    { Promotion. }
      if Length(LComputerMove) > 4 then
        LPromotionType := LComputerMove[5]
      else
      begin
      { Ne devrait pas arriver. }
        LPromotionType := 'q';
      end;
      case LPos.color of
        FALSE: LPieces[LPieceIndex].fg.LoadFromFile('images/w' + LPromotionType + '.bmp');
        TRUE:  LPieces[LPieceIndex].fg.LoadFromFile('images/b' + LPromotionType + '.bmp');
      end;
    end;

    LPieces[LPieceIndex].bg.Canvas.CopyRect(
      Rect(0, 0, CScale, CScale),
      FBitmap.Canvas,
      Rect(LPieces[LPieceIndex].xx, LPieces[LPieceIndex].yy, LPieces[LPieceIndex].xx + CScale, LPieces[LPieceIndex].yy + CScale)
    );
    FBitmap.Canvas.Draw(LPieces[LPieceIndex].xx, LPieces[LPieceIndex].yy, LPieces[LPieceIndex].fg);
    InvalidateRect(Handle, nil, FALSE);
  end;
  
  DoMove(LPos, LComputerMove, 'capablanca');
  WriteLn(ShowPosition(LPos));
  AfterMove;
end;

function TForm1.IsLegal(const AMove: string): boolean;
var
  LIndex: integer;
begin
  result := FALSE;
  for LIndex := 0 to High(LMoveList) do
    if AMove = Copy(LMoveList[LIndex], 1, 4) then
    begin
      result := TRUE;
      Break;
    end;
end;

procedure TForm1.DrawEmptyChessboard;
{$DEFINE GRAY}
const
  CColors: array[boolean] of TColor = {$IFDEF GRAY}(clLtGray, clMedGray){$ELSE}(clCream, clSkyBlue){$ENDIF};
var
  x, y: integer;
begin
 {FBitmap.LoadFromFile('images/board.bmp');}
  for x := 0 to 9 do
    for y := 0 to 7 do
    begin
      FBitmap.Canvas.Brush.Color := CColors[(x + y) mod 2 = 0];
      FBitmap.Canvas.FillRect(Rect(CScale * x, CScale * y, CScale * Succ(x), CScale * Succ(y)));
    end;
end;

procedure TForm1.CreatePieces(const AFen: string);
var
  i, j, k, x, y: integer;
  c: char;
begin
  i := 1;
  j := 0;
  x := 0;
  y := 0;
  while AFen[i] <> ' ' do
  begin
    c := AFen[i];
    case c of
      'P', 'R', 'N', 'A', 'B', 'Q', 'K', 'C', 'p', 'r', 'n', 'a', 'b', 'q', 'k', 'c':
        begin
          LPieces[j].xx := CScale * x;
          LPieces[j].yy := CScale * y;
          LPieces[j].fg := TBitmap.Create;
          LPieces[j].fg.Width := CScale;
          LPieces[j].fg.Height := CScale;
          case c of
            'P': LPieces[j].fg.LoadFromFile('images/wp.bmp');
            'R': LPieces[j].fg.LoadFromFile('images/wr.bmp');
            'N': LPieces[j].fg.LoadFromFile('images/wn.bmp');
            'A': LPieces[j].fg.LoadFromFile('images/wa.bmp');
            'B': LPieces[j].fg.LoadFromFile('images/wb.bmp');
            'Q': LPieces[j].fg.LoadFromFile('images/wq.bmp');
            'K': LPieces[j].fg.LoadFromFile('images/wk.bmp');
            'C': LPieces[j].fg.LoadFromFile('images/wc.bmp');
            'p': LPieces[j].fg.LoadFromFile('images/bp.bmp');
            'r': LPieces[j].fg.LoadFromFile('images/br.bmp');
            'n': LPieces[j].fg.LoadFromFile('images/bn.bmp');
            'a': LPieces[j].fg.LoadFromFile('images/ba.bmp');
            'b': LPieces[j].fg.LoadFromFile('images/bb.bmp');
            'q': LPieces[j].fg.LoadFromFile('images/bq.bmp');
            'k': LPieces[j].fg.LoadFromFile('images/bk.bmp');
            'c': LPieces[j].fg.LoadFromFile('images/bc.bmp');
          end;
          LPieces[j].fg.Transparent := TRUE;
          LPieces[j].bg := TBitmap.Create;
          LPieces[j].bg.Width := CScale;
          LPieces[j].bg.Height := CScale;
          LPieces[j].bg.Canvas.CopyRect(
            Rect(0, 0, CScale, CScale),
            FBitmap.Canvas,
            Rect(LPieces[j].xx, LPieces[j].yy, LPieces[j].xx + CScale, LPieces[j].yy + CScale)
          );
          FBitmap.Canvas.Draw(LPieces[j].xx, LPieces[j].yy, LPieces[j].fg);
          Inc(j);
          Inc(x);
        end;
      '1'..'9':
        begin
          if (c = '1') and (i < Length(AFen)) and (AFen[Succ(i)] = '0') then
          begin
            Inc(x, 10);
            Inc(i);
          end else
            for k := 0 to Ord(c) - Ord('1') do
              Inc(x);
        end;
      '/':
        begin
          x := 0;
          Inc(y);
        end;
    end;
    Inc(i);
  end;
end;

procedure TForm1.FreePieces;
var
  i: integer;
begin
  for i := 0 to 39 do
  begin
    if Assigned(LPieces[i].fg) then LPieces[i].fg.Free;
    if Assigned(LPieces[i].bg) then LPieces[i].bg.Free;
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
{$I version}
begin
  WriteLn(CAppInfo);

  FBitmap := TBitmap.Create;
  FBitmap.Width := 10 * CScale;
  FBitmap.Height := 8 * CScale;
  DrawEmptyChessboard;

 {Screen.Cursors[GRAB] := LoadCursorFromFile(PChar(ExtractFilePath(ParamStr(0)) + 'cursors\grab.cur'));
  Screen.Cursors[GRABBING] := LoadCursorFromFile(PChar(ExtractFilePath(ParamStr(0)) + 'cursors\grabbing.cur'));}
  Screen.Cursors[GRAB] := LoadCursorFromLazarusResource('grab');
  Screen.Cursors[GRABBING] := LoadCursorFromLazarusResource('grabbing');

  LFen := CCapablancaStartPos;
  CreatePieces(LFen);
  InitPosition(LPos, LFen);
  WriteLn(ShowPosition(LPos));

  AfterMove;

  FGameOver := FALSE;
end;

procedure TForm1.FormActivate(Sender: TObject);
begin
  Constraints.MinWidth := Width;
  Constraints.MinHeight := Height;
end;

procedure TForm1.miComputerPlaysClick(Sender: TObject);
begin
  miComputerPlaysWhite.Checked := miComputerPlaysWhite = Sender;
  miComputerPlaysBlack.Checked := miComputerPlaysBlack = Sender;
  miComputerDoesNotPlay.Checked := miComputerDoesNotPlay = Sender;

  if (miComputerPlaysWhite.Checked and not LPos.color
  or  miComputerPlaysBlack.Checked and LPos.color)
  and not FGameOver then
    ComputerMove;
end;

procedure TForm1.miNewGameClick(Sender: TObject);
begin
  FreePieces;
  DrawEmptyChessboard;
  LFen := CCapablancaStartPos;
  CreatePieces(LFen);
  InvalidateRect(Handle, nil, FALSE);
  InitPosition(LPos, LFen);
  WriteLn(ShowPosition(LPos));
  AfterMove;
  FGameOver := FALSE;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  FreePieces;
  FBitmap.Free;
end;

procedure TForm1.miAboutClick(Sender: TObject);
var
  LForm: TForm2;
begin
  LForm := TForm2.Create(nil);
  try
    LForm.ShowModal;
  finally
    LForm.Free;
  end;
end;

procedure TForm1.pnChessboardPaint(Sender: TObject);
var
  i, w, h: integer;
  c: char;
begin
  for i := 1 to 10 do
  begin
    c := Chr(i + Pred(Ord('A')));
    w := pnChessboard.Canvas.TextWidth(c);
    h := pnChessboard.Canvas.TextHeight(c);
    pnChessboard.Canvas.TextOut(36 * Pred(i) + 18 + (36 - w) div 2, (18 - h) div 2,            c);
    pnChessboard.Canvas.TextOut(36 * Pred(i) + 18 + (36 - w) div 2, (18 - h) div 2 + 288 + 18, c);
  end;
  for i := 1 to 8 do
  begin
    c := Chr(Ord('9') - i);
    w := pnChessboard.Canvas.TextWidth(c);
    h := pnChessboard.Canvas.TextHeight(c);
    pnChessboard.Canvas.TextOut((18 - w) div 2,            36 * Pred(i) + 18 + (36 - h) div 2, c);
    pnChessboard.Canvas.TextOut((18 - w) div 2 + 360 + 18, 36 * Pred(i) + 18 + (36 - h) div 2, c);
  end;
end;

procedure TForm1.miQuitClick(Sender: TObject);
begin
  Close;
end;

procedure TForm1.pbChessboardMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  i: integer;
begin
  if FGameOver then
    Exit;

  for i := 0 to 39 do
    if  (X >= LPieces[i].xx) and (X < LPieces[i].xx + CScale)
    and (Y >= LPieces[i].yy) and (Y < LPieces[i].yy + CScale) then
    begin
      LOldX := LPieces[i].xx;
      LOldY := LPieces[i].yy;
      LDragging := TRUE;
      pbChessboard.Cursor := GRABBING;
      LDX := X - LPieces[i].xx;
      LDY := Y - LPieces[i].yy;
      LPieceIndex := i;
      LUserMove := SquareToStr(LPieces[i].xx div CScale, 7 - LPieces[i].yy div CScale);
      Break;
    end;
end;

procedure TForm1.pbChessboardMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
  i: integer;
  LPieceOnSquare: boolean;
begin
  if LDragging then
  begin
    if X - LDX < 0 then X := LDX;
    if X - LDX > 9 * CScale then X := LDX + 9 * CScale;
    if Y - LDY < 0 then Y := LDY;
    if Y - LDY > 7 * CScale then Y := LDY + 7 * CScale;

    FBitmap.Canvas.Draw(LPieces[LPieceIndex].xx, LPieces[LPieceIndex].yy, LPieces[LPieceIndex].bg);
    LPieces[LPieceIndex].xx := X - LDX;
    LPieces[LPieceIndex].yy := Y - LDY;
    LPieces[LPieceIndex].bg.Canvas.CopyRect(
      Rect(0, 0, CScale, CScale),
      FBitmap.Canvas,
      Rect(LPieces[LPieceIndex].xx, LPieces[LPieceIndex].yy, LPieces[LPieceIndex].xx + CScale, LPieces[LPieceIndex].yy + CScale)
    );
    FBitmap.Canvas.Draw(LPieces[LPieceIndex].xx, LPieces[LPieceIndex].yy, LPieces[LPieceIndex].fg);
    InvalidateRect(Handle, nil, FALSE);
  end else
  begin
    LPieceOnSquare := FALSE;
    for i := 0 to 39 do
      if  (X >= LPieces[i].xx) and (X < LPieces[i].xx + CScale)
      and (Y >= LPieces[i].yy) and (Y < LPieces[i].yy + CScale) then
      begin
        LPieceOnSquare := TRUE;
        Break;
      end;
    if LPieceOnSquare then
      pbChessboard.Cursor := GRAB
    else
      pbChessboard.Cursor := CRDEFAULT;
  end;
end;

procedure TForm1.pbChessboardMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  i, xx, yy: integer;
  LRookCurrentX, LRookCurrentY, LKingTargetX, LRookTargetX, LRookIndex: integer;
  LCapturedPawnX, LCapturedPawnY: integer;
begin
  if LDragging then
  begin
    LDragging := FALSE;
    pbChessboard.Cursor := GRAB;

  { Coordonnées rectifiées de l'image. }
    xx := CScale * ((X - LDX + CScale div 2) div CScale);
    yy := CScale * ((Y - LDY + CScale div 2) div CScale);

    LUserMove := LUserMove + SquareToStr(xx div CScale, 7 - yy div CScale);

    if IsLegal(LUserMove) then
    begin
    { Coup légal.  }
      if IsCastling(LPos, LUserMove, LRookCurrentX, LRookCurrentY, LKingTargetX, LRookTargetX) then
      begin
      { Roque. }

        LRookCurrentY := 7 - LRookCurrentY;

      { Déplacement de l'image du roi. }
        FBitmap.Canvas.Draw(LPieces[LPieceIndex].xx, LPieces[LPieceIndex].yy, LPieces[LPieceIndex].bg);

        LPieces[LPieceIndex].xx := CScale * LKingTargetX;
        LPieces[LPieceIndex].yy := CScale * LRookCurrentY;

        LPieces[LPieceIndex].bg.Canvas.CopyRect(
          Rect(0, 0, CScale, CScale),
          FBitmap.Canvas,
          Rect(LPieces[LPieceIndex].xx, LPieces[LPieceIndex].yy, LPieces[LPieceIndex].xx + CScale, LPieces[LPieceIndex].yy + CScale)
        );
        FBitmap.Canvas.Draw(LPieces[LPieceIndex].xx, LPieces[LPieceIndex].yy, LPieces[LPieceIndex].fg);

      { Recherche de l'image de la tour. }
        for i := 0 to 39 do
          if  (LPieces[i].xx = CScale * LRookCurrentX)
          and (LPieces[i].yy = CScale * LRookCurrentY) then
          begin
            LRookIndex := i;
            Break;
          end;

      { Déplacement de l'image de la tour. }
        FBitmap.Canvas.Draw(LPieces[LRookIndex].xx, LPieces[LRookIndex].yy, LPieces[LRookIndex].bg);

        LPieces[LRookIndex].xx := CScale * LRookTargetX;
        LPieces[LRookIndex].yy := CScale * LRookCurrentY;

        LPieces[LRookIndex].bg.Canvas.CopyRect(
          Rect(0, 0, CScale, CScale),
          FBitmap.Canvas,
          Rect(LPieces[LRookIndex].xx, LPieces[LRookIndex].yy, LPieces[LRookIndex].xx + CScale, LPieces[LRookIndex].yy + CScale)
        );
        FBitmap.Canvas.Draw(LPieces[LRookIndex].xx, LPieces[LRookIndex].yy, LPieces[LRookIndex].fg);
        InvalidateRect(Handle, nil, FALSE);
      end else
      begin
      { Autres coups. }

        FBitmap.Canvas.Draw(LPieces[LPieceIndex].xx, LPieces[LPieceIndex].yy, LPieces[LPieceIndex].bg);

        LPieces[LPieceIndex].xx := xx;
        LPieces[LPieceIndex].yy := yy;

        if IsEnPassantCapture(LPos, LUserMove, LCapturedPawnX, LCapturedPawnY) then
        begin
        { Prise en passant. }
          LCapturedPawnY := 7 - LCapturedPawnY;
        { Effacement de l'image du pion capturé. }
          for i := 0 to 39 do
            if  (LPieces[i].xx = CScale * LCapturedPawnX)
            and (LPieces[i].yy = CScale * LCapturedPawnY) then
            begin
              FBitmap.Canvas.Draw(LPieces[i].xx, LPieces[i].yy, LPieces[i].bg);
              LPieces[i].xx := -1;
              LPieces[i].yy := -1;
              Break;
            end;
        end else
        { Effacement de l'image d'une éventuelle pièce capturée. }
          for i := 0 to 39 do
            if (i <> LPieceIndex)
            and (LPieces[i].xx = LPieces[LPieceIndex].xx)
            and (LPieces[i].yy = LPieces[LPieceIndex].yy) then
            begin
              FBitmap.Canvas.Draw(LPieces[i].xx, LPieces[i].yy, LPieces[i].bg);
              LPieces[i].xx := -1;
              LPieces[i].yy := -1;
              Break;
            end;

        if IsPromotion(LPos, LUserMove) then
        begin
        { Promotion. }
          case LPos.color of
            FALSE: LPieces[LPieceIndex].fg.LoadFromFile('images/wq.bmp');
            TRUE:  LPieces[LPieceIndex].fg.LoadFromFile('images/bq.bmp');
          end;
          LUserMove := LUserMove + 'q';
        end;

        LPieces[LPieceIndex].bg.Canvas.CopyRect(
          Rect(0, 0, CScale, CScale),
          FBitmap.Canvas,
          Rect(LPieces[LPieceIndex].xx, LPieces[LPieceIndex].yy, LPieces[LPieceIndex].xx + CScale, LPieces[LPieceIndex].yy + CScale)
        );
        FBitmap.Canvas.Draw(LPieces[LPieceIndex].xx, LPieces[LPieceIndex].yy, LPieces[LPieceIndex].fg);
        InvalidateRect(Handle, nil, FALSE);
      end;

      DoMove(LPos, LUserMove, 'capablanca');
      WriteLn(ShowPosition(LPos));
      AfterMove;
    end else
    begin
    { Si le coup n'est pas légal, on remet la pièce à sa place. }
      FBitmap.Canvas.Draw(LPieces[LPieceIndex].xx, LPieces[LPieceIndex].yy, LPieces[LPieceIndex].bg);
      LPieces[LPieceIndex].xx := LOldX;
      LPieces[LPieceIndex].yy := LOldY;
      LPieces[LPieceIndex].bg.Canvas.CopyRect(
        Rect(0, 0, CScale, CScale),
        FBitmap.Canvas,
        Rect(LPieces[LPieceIndex].xx, LPieces[LPieceIndex].yy, LPieces[LPieceIndex].xx + CScale, LPieces[LPieceIndex].yy + CScale)
      );
      FBitmap.Canvas.Draw(LPieces[LPieceIndex].xx, LPieces[LPieceIndex].yy, LPieces[LPieceIndex].fg);
      InvalidateRect(Handle, nil, FALSE);

      LUserMove := EmptyStr;
    end;
  end;
end;

procedure TForm1.pbChessboardPaint(Sender: TObject);
begin
  pbChessboard.Canvas.Draw(0, 0, FBitmap);
end;

initialization
{$I cursors.lrs}
end.
