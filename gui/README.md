# Durandal GUI

## Description

GUI for playing Capablanca chess against *Durandal*.

## Compilation

*Durandal GUI* is a Lazarus project.

## Credits

- Pieces pictures by [Reinhard Scharnagl](https://www.chessvariants.com/contests/10/crc.html).
- Sword icon by [Zush Design](https://opengameart.org/content/pixel-sword-3).

## Screenshot

![Durandal GUI](screenshot.png)
