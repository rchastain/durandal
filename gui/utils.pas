
unit utils;

interface

uses
  Chess;

function SquareToStr(const X, Y: integer): string;
procedure StrToSquare(const AStr: string; out X, Y: integer);

function IsCastling(const APos: TPosition; const AMove: string; out ARookCurrentX, ARookCurrentY, AKingTargetX, ARookTargetX: integer): boolean;
function IsEnPassantCapture(const APos: TPosition; const AMove: string; out ACapturedPawnX, ACapturedPawnY: integer): boolean;
function IsPromotion(const APos: TPosition; const AMove: string): boolean;

implementation

procedure DecodeMove(const AMove: string; out AFrom, ATo: integer);
var
  LPromo: TPieceType;
begin
  Chess.DecodeMove(AMove, AFrom, ATo, LPromo);
end;

function SquareToStr(const X, Y: integer): string;
begin
  result := Chr(X + Ord('a')) + Chr(Y + Ord('1'));
end;

procedure StrToSquare(const AStr: string; out X, Y: integer);
begin
  X := Ord(AStr[1]) - Ord('a');
  Y := Ord(AStr[2]) - Ord('1');
end;

function IsCastling(const APos: TPosition; const AMove: string; out ARookCurrentX, ARookCurrentY, AKingTargetX, ARookTargetX: integer): boolean;
var
  LFrom, LTo: integer;
begin
  DecodeMove(AMove, LFrom, LTo);
  result :=
    (APos.board[LFrom] = 'K') and (APos.board[LTo] = 'R') or
    (APos.board[LFrom] = 'k') and (APos.board[LTo] = 'r');
  if result then
  begin
    ARookCurrentX := (LTo - 35) div 15;
    ARookCurrentY := (LTo - 35) mod 15;
    if LTo > LFrom then
    begin
      AKingTargetX := 8;
      ARookTargetX := 7;
    end else
    begin
      AKingTargetX := 2;
      ARookTargetX := 3;
    end;
  end else
  begin
    ARookCurrentX := -1;
    ARookCurrentY := -1;
    AKingTargetX  := -1;
    ARookTargetX  := -1;
  end;
end;

function IsEnPassantCapture(const APos: TPosition; const AMove: string; out ACapturedPawnX, ACapturedPawnY: integer): boolean;
var
  LFrom, LTo: integer;
begin
  DecodeMove(AMove, LFrom, LTo);
  result :=
    (
      (APos.board[LFrom] = 'P') or
      (APos.board[LFrom] = 'p')
    )
    and (
      (LFrom - 35) div 15 <>
      (LTo   - 35) div 15
    )
    and (APos.board[LTo] = '.');
  if result then
  begin
    ACapturedPawnX := (LTo   - 35) div 15;
    ACapturedPawnY := (LFrom - 35) mod 15;
  end else
  begin
    ACapturedPawnX := -1;
    ACapturedPawnY := -1;
  end;
end;

function IsPromotion(const APos: TPosition; const AMove: string): boolean;
var
  LFrom, LTo: integer;
begin
  DecodeMove(AMove, LFrom, LTo);
  result :=
    (APos.board[LFrom] = 'P') and ((LTo - 35) mod 15 = 7) or
    (APos.board[LFrom] = 'p') and ((LTo - 35) mod 15 = 0);
end;

end.
